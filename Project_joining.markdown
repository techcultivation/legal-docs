# Projects joining

This document describes the requirements and procedures for a project to join the Center for the Cultivation of Technology (CCT).


## Candidate project requirements

One of the main goals of the Center for the Cultivation of Technology is minimising project’s administrative overhead. Therefore it aims at being as light-weight as possible itself in its requirements and obligations from the project.

Still, in order to properly represent and protect a project, we need it to meet some minimum requirements.

A CCT member project has to be released under a free and open license. For further details see [Outbound licensing][outbound] document.

The project and all its representatives have to agree and comply with CCT’s [Mission][mission] and [Bylaws][bylaws] as well as general professional integrity, described in the [Code of conduct][coc].

It should also be noted that the relationship is non-exclusive and that the project may work with other organisations, even other fiscal sponsors, to complement the services offered by the CCT. In such case, the project should notify the Center for the Cultivation of Technology, which other organisations it already joined or uses, or wants to join or use; as well as its relationship with them. For such the project does not need an explicit agreement from the CCT, but should this be counter to the [Mission][mission], [Bylaws][bylaws] or the [Code of conduct][coc], the CCT may ask the project to fix this issue.

[outbound]: /Outbound_licensing.markdown
[coc]: /Code_of_conduct.markdown
[mission]: https://techcultivation.org/docs/overview.html#mission
[bylaws]: https://techcultivation.org/docs/bylaws.html


## Process

### First informal contact

Generally the process starts with an informal discussion between representatives of the project and board members of the Center for the Cultivation of Technology. Such discussions are of purely informational nature and do not constitute any obligation to either party.

For more information on formal and informal communication see the [Internal decisions and communication][communication] document.

[communication]: /Internal_decisions_and_communication.markdown


### Formal application

A project formally applies by sending a simple e-mail to <contact@techcultivation.org> expressing interest to join.

Upon receival, the CCT makes some quick checks to see whether the project qualifies or not. If it does, the CCT notifies the project that it found it eligible to join.

At the latest at this stage, the project has to select a primary contact person for all things related to its relationship with the Center for the Cultivation of Technology. This person – the Project Representative – should have all the rights necessary to represent and enter into agreements for the project. How the Project Representative is appointed by the project is left entirely to the project itself.

Next, both the Center for the Cultivation of Technology and the project issue a public notice (e.g. on their websites) stating that the project is in the process of joining the CCT and who the selected Project Representative is. This public notice must also allow for interested project members to object to either the Project Representative or the project joining the CCT. This is governed by the rules proscribed in the [Public consultation][consultation] document, where after the public notice, the Public request phase is started. The public consultation procudure has to finish before the formal agreement (below) can be signed and entered into, but for the sake of time efficiency, does not prevent the project and CCT to formally discuss the content and draft the agreement.

[consultation]: /Public_consultation.markdown


### Formal agreement

After the project has formally applied and the Center for the Cultivation of Technology has notified it that the project is eligible to join, the project and the CCT shall agree in writing regarding the following:

- which services does the Center for the Cultivation of Technology provide to the project (see the current online list of [services][services] offered for reference);
- the payment that the Center for the Cultivation of Technology receives for its services – as a guiding rule, for standard services 10% of all incoming donations and grants should be appropriate and is therefore the default;
- that both parties agree to follow [the legal and documents rules of the Center for the Cultivation of Technology][legal-docs];
- which are the formal contacts on each party’s side.

[services]: https://techcultivation.org/docs/overview.html#our-services
[legal-docs]: https://gitlab.com/techcultivation/legal-docs

### On-boarding

After both parties sign the formal agreement, the Center for the Cultivation of Technology sets up all the needed accounts and access to its platfrom and other services to the Project Representative.

On the platform itself the Project Representative may delegate power and/or grant specific roles to to other members of the project.


## Freedom to leave

At any stage the project is free to decide to leave the Center for the Cultivation of Technology. The procedure and details are proscribed in the [Project leaving][leaving] document.

[leaving]: /Project_leaving.markdown

---

Written in 2016 by Matija Šuklje <hook@techcultivation.org> on behalf of the Center for the Cultivation of Technology <https://techcultivation.org>.

To the extent possible under law, the author(s) have dedicated all copyright and related and neighboring rights to this document to the public domain worldwide.

You should have received a copy of the CC0 Public Domain Dedication along with this document. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>. 

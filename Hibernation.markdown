# Hibernation of a project

In the event that a cer­tain pro­gramme has ceased de­vel­op­ment and is no longer able to sus­tain its op­er­a­tion, the pro­gram­me can be sent in­to hi­ber­na­tion. Any as­sets held by the Center for the Cultivation of Technology on it­s be­half shall be treat­ed ac­cord­ing to the pro­ce­dure de­fined in this doc­u­men­t. The Center for the Cultivation of Technology will han­dle the lega­cy of a project on a “best ­ef­fort” ba­sis, and with the in­ten­tion to serve the pro­gram­me’s wider com­mu­ni­ty.


## Hibernation process

The fol­low­ing course of ac­tion (in this pre­cise or­der) will be tak­en, with each step be­ing pub­licly an­nounced on the rel­e­vant of­fi­cial pub­li­ca­tion chan­nels of the Center for the Cultivation of Technology (e.g. its web­site):

1. A copy of all on­line as­sets stored on ex­ter­nal servers shall be archived by the Center for the Cultivation of Technology, where pos­si­ble in­clud­ing soft­ware nec­es­sary to ­main­tain them.
2. If a project has made internal pro­vi­sions or reg­u­la­tions re­gard­ing cer­tain as­sets in case of the ter­mi­na­tion of the project, those pro­vi­sion­s have to be hon­oured pro­vid­ed such is in com­pli­ance with the [Mission][mission] and [Bylaws][bylaws] of the the Center for the Cultivation of Technology at the time the project joined it.
3. All re­main­ing as­sets shall be trans­ferred to a place-hold­er project di­rect­ly un­der the aegis of the Center for the Cultivation of Technology.

[mission]: https://techcultivation.org/docs/overview.html#mission
[bylaws]: https://techcultivation.org/docs/bylaws.html


## Reactivation of a hibernated project

As­sets placed in hi­ber­na­tion may be re­ac­ti­vat­ed pend­ing strict con­di­tion­s, in­clud­ing any con­di­tions set by the project it­self pri­or to its hi­ber­na­tion in its internal provisions or regulations. An­oth­er project may re­quest the Center for the Cultivation of Technology to adopt some or all of the as­sets kept in hi­ber­na­tion, or even to re­open the as­sets as a new project al­to­geth­er. Any such re­quest shall be sub­ject to a pub­lic con­sul­ta­tion round (see [Pub­lic Con­sul­ta­tion][public_consultation] document).

[public_consultation]: /Public_consultation.markdown

## Public notification

The Center for the Cultivation of Technology shall place no­ti­fi­ca­tions on the rel­e­vant of­fi­cial pub­li­ca­tion chan­nels of the Center for the Cultivation of Technology (e.g. its web­site), where ­pos­si­ble to in­form in­ter­est­ed stake­hold­ers — such as, for ex­am­ple, users and de­vel­op­ers of the project’s work — about the hi­ber­na­tion sta­tus of the project, as well as any re­quests to adopt hi­ber­nat­ed as­sets in oth­er projects.

Note that the Center for the Cultivation of Technology may re­lo­cate hi­ber­nat­ed as­sets to an ex­ter­nal not-­for-prof­it en­ti­ty at any point if such is nec­es­sary to pro­tec­t it­self and its oth­er projects against le­gal ac­tion in­volv­ing hi­ber­nat­ed as­set­s.


---

Written in 2017 by Matija Šuklje <hook@techcultivation.org> on behalf of the Center for the Cultivation of Technology <https://techcultivation.org> as a modification of the [Hibernation of assets DRACC of the \[The Commons Conservancy\]][hibernation_tcc], authored in 2017 by Michiel Leenaars <michiel@nlnet.nl> and Matija Šuklje <matija@suklje.name>.

This work is licensed under the Creative Commons Attribution 4.0 International License. To view a copy of this license, visit <http://creativecommons.org/licenses/by/4.0/>.

[hibernation_tcc]: http://dracc.commonsconservancy.org/0005/

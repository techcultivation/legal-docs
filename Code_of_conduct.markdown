# Code of conduct


## Introduction

As an ethical or­gan­i­sa­tion and as an open com­mu­ni­ty our guid­ing prin­ci­ples are respect for the in­di­vid­u­al, the right to self­-de­ter­mi­na­tion, the right to ­make informed de­ci­sions and the spir­it of co­op­er­a­tion. We aim to be a safe and in­clu­sive en­vi­ron­men­t, fos­ter­ing a rich di­ver­si­ty of hu­man tal­en­t. The _Code of con­duct_ is a cod­i­fi­ca­tion of the eth­i­cal and pro­fes­sion­al be­hav­iour ex­pect­ed of those en­gag­ing with our ac­tiv­i­ties.

The _Code of con­duct_ is based (mu­tatis mu­tan­dis) on the 2016 ver­sion of the [IEEE Code of Ethics][coc_ieee], as es­tab­lished by the In­sti­tute of Elec­tri­cal and ­Elec­tron­ics En­gi­neers through its Ethics and Mem­ber Con­duct Com­mit­tee. ­Main­tain­ing the Code of con­duct is the re­spon­si­bil­i­ty of the board of the Center for the Cultivation of Technology, but the de­mand, am­bi­tion and knowl­edge to do the very best we can is shared through­out our com­mu­ni­ty. Any­one is there­fore en­cour­aged ­to con­trib­ute sugges­tions how to im­prove the Code of con­duc­t.


## Code of conduct

We ex­pect the fol­low­ing be­hav­iour to be the norm with­in the Center for the Cultivation of Technology, and thus com­mit our­selves:

1. to ac­cept re­spon­si­bil­i­ty in mak­ing de­ci­sions con­sis­tent with the safe­ty, health, and wel­fare of the pub­lic, and to dis­close prompt­ly fac­tors that might en­dan­ger the pub­lic or the en­vi­ron­men­t;
1. to avoid re­al or per­ceived con­flicts of in­ter­est when­ev­er pos­si­ble, and to dis­close them to af­fect­ed par­ties when they do ex­ist;
1. to be hon­est and re­al­is­tic in stat­ing claims or es­ti­mates based on avail­able ­data;
1. to re­ject bribery in all its form­s;
1. to im­prove the un­der­stand­ing of tech­nol­o­gy; its ap­pro­pri­ate ap­pli­ca­tion, and po­ten­tial con­se­quences;
1. to main­tain and im­prove per­son­al and col­lec­tive tech­ni­cal com­pe­tence and to un­der­take tech­no­log­i­cal tasks for oth­ers on­ly if qual­i­fied by train­ing or ­ex­pe­ri­ence, or af­ter full dis­clo­sure of per­ti­nent lim­i­ta­tion­s;
1. to seek, ac­cep­t, and of­fer hon­est crit­i­cism of tech­ni­cal work, to ac­knowl­edge and cor­rect er­rors, and to cred­it prop­er­ly the con­tri­bu­tions of other­s;
1. to treat fair­ly all per­sons and to not en­gage in acts of dis­crim­i­na­tion based on race, re­li­gion, gen­der, dis­abil­i­ty, age, na­tion­al orig­in, sex­u­al ori­en­ta­tion, gen­der iden­ti­ty, or gen­der ex­pres­sion;
1. to avoid in­jur­ing oth­er­s, their prop­er­ty, rep­u­ta­tion, or em­ploy­ment by false or ma­li­cious ac­tion;
1. to as­sist oth­ers in their pro­fes­sion­al de­vel­op­ment and to sup­port them in­ ­fol­low­ing this code of ethic­s.

---

Written in 2017 by Matija Šuklje <hook@techcultivation.org> on behalf of the Center for the Cultivation of Technology <https://techcultivation.org> as a modification of the [Code of Conduct of \[The Commons Conservancy\]][coc_tcc], authored in 2016 by Michiel Leenaars <michiel@nlnet.nl> and Matija Šuklje <matija@suklje.name>.

This work is licensed under the Creative Commons Attribution 4.0 International License. To view a copy of this license, visit <http://creativecommons.org/licenses/by/4.0/>.

[coc_tcc]: http://dracc.commonsconservancy.org/0015/
[coc_ieee]: http://www.ieee.org/about/corporate/governance/p7-8.html

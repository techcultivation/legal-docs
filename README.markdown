# What

These are the internal legal and governance documents for [The Center for the Cultivation of Technology][cct].

[cct]: https://techcultivation.org/


# Contribute

The main repository is for it resides here:

<https://gitlab.com/techcultivation/legal-docs>

Bugs, issues and tasks are handled through the RedMine instance:

<https://issues.techcultivation.org/projects/cct-orga>

All contributions should be given under [the CC0 Public Domain Dedication][CC0-1.0]. For convenience, you may use the `LICENSE_header_snippet.*`.

[CC0-1.0]: http://creativecommons.org/publicdomain/zero/1.0/

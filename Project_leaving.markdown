# Project leaving

This document describes the procedure and other details should a project decide to leave the Center for the Cultivation of Technology (CCT).


## Reasons for leaving

If a project decides that the Center for the Cultivation of Technology is not the right host for it any more, – whether because it wants to set up its own organisation or whatever other reason – it may leave at any point.

The Center for the Cultivation of Technology may only transfer assets to other organisations in public interest, and this should be taken into account also when a project is leaving.

It should also be noted that the relationship is non-exclusive and that the project may work with other organisations, even other fiscal sponsors, to complement the services offered by the CCT. Therefore if a project joins a different organisation, this, by itself, does not mean it has to leave the CCT.


## Assets

Unless otherwise agreed upon in writing, the Center for the Cultivation of Technology shall stop accepting donations and other assets for the project and transfer these in a reasonable time frame to the project. Further details are described below.

### Financial assets

For financial assets, the Center for the Cultivation of Technology shall stop accepting donations in the project’s name and either transfer the donation accounts to the project or remove such accounts, depending on what is more appropriate for both parties. The already gathered financial assets shall be transfered to the project’s own accounts no later than 30 days after the project has also notified the the Center for the Cultivation of Technology of all the information needed for such a transfer to legally happen.

Should the project still have any due payments – whether to the Center for the Cultivation of Technology – or otherwise, the CCT may substract these from the amount being transferred in order to carry out these payments.

### Physical assets

Physical assets such as hardware equipment shall be transferred to the address provided by the project in no later than 30 days.

If some of the physical assets are shared between different projects (e.g. shared servers), the Center for the Cultivation of Technology or its remaining member projects may instead buy the share of the project leaving.

### Intangible assets

Should any intangible assets be aggregated through the Center for the Cultivation of Technology, such assets shall be transferred:

- to the new organisation that is to hold the aggregate intangible assets for the project in the future; or should that not exist,
- to the original authors themselves – effectively returning to each author the assets they contributed themselves.

In any case – and this is unwaivable even in a written agreement –, the Center for the Cultivation of Technology reserves the right to hold a copy of the project’s intangible assets and continue to provide it to the public under a free and open license (see [Outbound licensing][outbound] document for licensing details).

[outbound]: Outbound_licensing.markdown


## Leaving procedure

### Formal notification

A project formally starts the procedure to leave when the Project Representative sends the project’s intention to leave to their formal contact at the Center for the Cultivation of Technology.

In this notice, the project should also name the new organisation that should receive the assets and provide the needed information for the Center for the Cultivation of Technology to check whether the transfer to the new organisation is compatible with the mission and bylaws of the Center for the Cultivation of Technology.

For more information on formal and informal communication see the [Internal decisions and communication][communication] document.

[communication]: /Internal_decisions_and_communication.markdown


### Formal agreement

After the project has formally notified the Center for the Cultivation of Technology and the Center for the Cultivation of Technology has clarified that the transfer to the desired new organisation is not against the mission and bylaws of the CCT, the parties (project, CCT, and the new organisation) sign an agreement, which includes at least the agreement that the project is leaving (and transferring its assets to the new organisation), and may also include further details of the leaving and transfer.


---

Written in 2017 by Matija Šuklje <hook@techcultivation.org> on behalf of the Center for the Cultivation of Technology <https://techcultivation.org>.

To the extent possible under law, the author(s) have dedicated all copyright and related and neighboring rights to this document to the public domain worldwide.

You should have received a copy of the CC0 Public Domain Dedication along with this document. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>. 


# Communication & internal decisions

This document sets forth the rules for formal communication between the project and the Center for the Cultivation of Technology.

Projects that are members of the the Center for the Cultivation of Technology operate un­der their own re­spon­si­bil­i­ty. The Center for the Cultivation of Technology seeks no op­er­a­tional in­volve­ment in­side it­s member projects, and lim­its it­self to servicing the projects within the bound­aries of the mis­sion of the Center for the Cultivation of Technology.

The pro­ce­dure as de­scribed in this doc­u­ment is there­fore on­ly in­tend­ed to pro­vide ad­di­tion­al safe­guards and trans­paren­cy, by us­ing a light-weight and non-in­va­sive method of so­cial con­trol and au­ditabil­i­ty.


## Types of communication channels


### Formal communication

All formal communication between the project and the CCT shall be made in the English language and take place either via e-mail or the CCT platform.

Decisions that influence the relationship between the project and the CCT have to be made using formal communication. This is needed in order to have a tracable document and decision path, so even after several years it can be proven why and how a decision was made.

When a project joins the the Center for the Cultivation of Technology, a func­tional e-mail dis­tri­bu­tion list is cre­at­ed by the Center for the Cultivation of Technology for com­mu­ni­ca­tion be­tween the CCT and the project.

Al­l of­fi­cials and ob­servers of the project must have at least one valid email address added to this mail­ing list. Peo­ple on the mail­ing list are strongly recommended to use an e-mail ac­count that of­fers pro­tec­tion against iden­ti­ty theft through use of ap­pro­pri­ate stan­dards (such as [SPF][spf], [D­KIM][dkim] and [D­MAR­C][dmarc]).

All for­mal mes­sages sent from the project to the CCT and vice ver­sa have to either _a)_ be sen­t ­to or rout­ed through this list; or _b)_ be made through the CCT  platform for functions that the platform allows. In both cases such formal communication will be archived by the CCT.

[spf]: https://tools.ietf.org/html/rfc7208
[dkim]: https://tools.ietf.org/html/rfc7489
[dmarc]: https://tools.ietf.org/html/rfc6376


### Informal communication

The CCT may for practical reasons also run other communication channels, such as mailing lists and IRC channels. The communication on these is purely of informal nature and does not constitute any legally binding contract or promise from either party.

Amongst others, such informal channels are:

- [#techcultivation on irc.OFTC.net](irc://irc.oftc.net/#techcultivation) IRC channel
- [CCT Lounge](https://lists.techcultivation.org/mailman/listinfo/cct-lounge) mailing list


## Project-internal decisions

Projects operate under their own responsibility even after they join the CCT. A project continues to make decisions regarding how it wants to continue developing and governing itself.

Such decisions the project may make via any communication and decision system that it uses internally.


## Decisions relating to CCT

Financial decisions that are covered by the CCT platform’s feature set, can be made through the CCT platform directly. Unless stated otherwise, the project’s decisions and instructions submited through the CCT platform will be automatically applied. It is the project’s responibility to ensure that the correct Project Representatives are assigned the correct corresponding roles and permissions within the CCT platform.

For all other for­mal de­ci­sions to be made, an authorised Project Representative must send it to the ­mail­ing list reserved for the project’s formal communication. If no ob­jec­tion is made ­by any­one on the list with­in 72 hours, un­less an­oth­er pro­ce­dure is de­fined with­in the statutes or reg­u­la­tions of the project itself, the de­ci­sion will be fi­nal, and unless it this goes against the CCT’s mission and statutes, the CCT will carry out the resulting action.


---

Written in 2017 by Matija Šuklje <hook@techcultivation.org> on behalf of the Center for the Cultivation of Technology <https://techcultivation.org> as a modification of the [DRACC 0004 “Decision handling procedure” \[The Commons Conservancy\]][decision], authored in 2017 by Michiel Leenaars <michiel@nlnet.nl> and Matija Šuklje <matija@suklje.name>.

This work is licensed under the Creative Commons Attribution 4.0 International License. To view a copy of this license, visit <http://creativecommons.org/licenses/by/4.0/>.

[decision]: https://dracc.commonsconservancy.org/0004/

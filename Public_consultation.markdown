# Public consultation

Projects with­in the Center for the Cultivation of Technology are un­der their own re­spon­si­bil­i­ty, but oc­ca­sion­al­ly ­con­sult­ing the project’s stake­hold­ers at large (which in­cludes the gen­er­al pub­lic) is de­sired or even re­quired. This doc­u­ment pre­scribes how such a pub­lic ­con­sul­ta­tion shall be han­dled. The time frames men­tioned in this doc­u­ment for each of the dif­fer­ent steps must be kep­t, un­less oth­er time frames are ­ex­plic­it­ly spec­i­fied and mo­ti­vat­ed in a reg­u­la­tion that pre­scribes or re­quest­s a public consultation.


## Public Consultation procedure


### Initiation phase

When a project wants or is required to ­con­duct a public consultation, the Project Representative shall send a writ­ten re­quest to the Center for the Cultivation of Technology with a de­tailed pro­pos­al of what the projects wants or needs to gath­er public consultation on. If a project has made provisions in its internal requlations re­gard­ing cer­tain as­sets or cer­tain pro­ce­dures, those pro­vi­sions must be in­di­cat­ed in the re­quest.

A con­fir­ma­tion of re­ceipt shall be sent by the Center for the Cultivation of Technology to the Project Representative(s) in­volved and to the ini­tia­tors of the pub­lic con­sul­ta­tion pro­ce­dure, should they be oth­er than its Project Representative.


### Public request phase

The Center for the Cultivation of Technology shall pub­lish the full re­quest on the rev­e­lant of­fi­cial pub­li­ca­tion chan­nel(s) of the CCT and the project (e.g. its web­sites), and open a 28 day pub­lic con­sul­ta­tion win­dow open to all stake­hold­ers — such as, for ex­am­ple, users and de­vel­op­ers of the project’s work. Re­spon­dents to the pub­lic con­sul­ta­tion may mark their re­spons­es as pub­lic or pri­vate, in case they have rea­sons to keep­ their re­sponse con­fi­den­tial in or­der to pro­tect their pri­va­cy. The Center for the Cultivation of Technology shall take rea­son­able mea­sures to pro­tect the pri­va­cy of the re­spon­dents who asked for their re­spons­es to be treat­ed con­fi­den­tial­ly.

If no con­test or rea­son­able ob­jec­tion is re­ceived through the pub­lic ­con­sul­ta­tion, the de­ci­sion is au­to­mat­i­cal­ly deemed as favourable and the Center for the Cultivation of Technology shall with­out un­due de­lay pub­lish its favourable de­ci­sion.


### Objection phase

If there are sig­nif­i­cant and rea­son­able ob­jec­tion­s, the ini­tia­tors of the pub­lic con­sul­ta­tion pro­ce­dure shall be asked to re­spond to each of these ob­jec­tions and giv­en rea­son­able time for their re­spon­se, with a min­i­mum of 14 days.

The re­sponse of the ini­tia­tors of the pub­lic con­sul­ta­tion pro­ce­dure to any ­con­tes­ta­tions or ob­jec­tions shall be sent to the in­di­vid­u­als or projects that made them, which in turn will be giv­en rea­son­able time for their re­spon­se, with a miminum of 14 days, to re­spond.


### Public decision phase

With­in 14 days af­ter the pub­lic con­sul­ta­tion has end­ed, de­pend­ing on the out­come of the pub­lic con­sul­ta­tion, the Center for the Cultivation of Technology shall ei­ther _a)_ pub­lish its de­ci­sion de­tail­ing how it will ex­e­cute the ac­cept­ed pro­pos­al; or _b)_ its own pro­posed res­o­lu­tion pro­ce­dure for any re­main­ing is­sues. Un­til all is­sues are re­solved, the pub­lic con­sul­ta­tion pro­ce­dure and all pro­ce­dures that de­pend on it will be for­mal­ly halt­ed. As ­soon as all is­sues are re­solved, said pro­ce­dures will con­tin­ue.

When per­mis­sion from the the Center for the Cultivation of Technology is grant­ed, the re­quired or­gan­i­sa­tion­al, le­gal and tech­ni­cal in­fra­struc­ture for the new si­t­u­a­tion shall be brought in­to readi­ness.

The Center for the Cultivation of Technology shall pub­lish its res­o­lu­tion on the rel­e­vant of­fi­cial pub­li­ca­tion chan­nel(s) of the CCT and the project (e.g. its web­sites). Any and all rel­e­vant in­for­ma­tion on these chan­nels shall be up­dat­ed to re­flect the new sit­u­a­tion. The nec­es­sary changes in­ in­fras­truc­ture set­up which fol­low the de­ci­sion (such as a re­di­rect­ion of we­b ­pages, or adding point­ers to the new projects) shall be made.

---

Written in 2017 by Matija Šuklje <hook@techcultivation.org> on behalf of the Center for the Cultivation of Technology <https://techcultivation.org> as a modification of the [Public Consultation DRACC of the \[The Commons Conservancy\]][consultation_tcc], authored in 2017 by Michiel Leenaars <michiel@nlnet.nl> and Matija Šuklje <matija@suklje.name>.

This work is licensed under the Creative Commons Attribution 4.0 International License. To view a copy of this license, visit <http://creativecommons.org/licenses/by/4.0/>.

[consultation_tcc]: http://dracc.commonsconservancy.org/0005/
